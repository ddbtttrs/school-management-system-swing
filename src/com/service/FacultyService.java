package com.service;

import java.util.List;

import com.model.Admin;
import com.model.Faculty;

public interface FacultyService {

	void addUser(Faculty a);
	void deleteUser(int id);
	List<Faculty> getAllUser();
	
}
