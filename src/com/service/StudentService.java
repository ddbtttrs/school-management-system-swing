package com.service;

import java.sql.Connection;
import java.util.List;

import com.model.Student;

public interface StudentService{

	void addStudent(Student s);
	void deleteStudent(int id);
	List<Student> getAllStudent();
	void updateStudent(int id, String field, String value);
	
}
