package com.service;

import java.util.List;

import com.model.Admin;

public interface AdminService {

	void addUser(Admin a);
	void deleteUser(int id);
	List<Admin> getAllUser();
	
}
