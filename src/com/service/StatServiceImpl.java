package com.service;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import com.db.DbOps;
import com.db.DbService;
import com.model.Student;

public class StatServiceImpl implements StatService {

	DbService db = new DbOps();
	DecimalFormat df = new DecimalFormat("#.00");
	
	@Override
	public void avrageYearInMajor(String major) {
		List<Student> slist = new  LinkedList<>();
		String query = "select * from student";
		ResultSet rs;
		rs = db.getUser(query);
		
		try {
			while(rs.next()) {
				
				 Student s = new Student();
				 s.setId(Integer.parseInt(rs.getString("id")));
				 s.setName(rs.getString("name"));
				 s.setAge(Integer.parseInt(rs.getString("age")));
				 s.setMajor(rs.getString("major"));
				 s.setUsername(rs.getString("username"));
				 s.setPassword(rs.getString("password"));
				 s.setRole(rs.getString("role"));
				 slist.add(s);
				  
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		
		//Operation 
		int count = 0;
		float avg = 0.0f;
		int total_student = 0;
		for (Student s : slist) {
			
			if(s.getMajor().equals(major)) {
				total_student++;
				count += s.getAge();
				avg =  (float)count/(total_student);				
			}		
		}		
		System.out.println("Avrage Year of Student in " + major +" Major = "+ df.format(avg) + "\nTotal Student in "+major+" Major = "+ total_student+"\n");
	}

	@Override
	public void totalStudentInMajor() {
		// TODO Auto-generated method stub
		
	}

}
