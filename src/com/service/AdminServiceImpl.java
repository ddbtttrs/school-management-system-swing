package com.service;

import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.db.DbOps;
import com.db.DbService;
import com.model.Admin;

public class AdminServiceImpl implements AdminService {

	Scanner sc = new Scanner(System.in);
	
	@Override
	public void addUser(Admin admin) {
		
		DbService db = new DbOps();		
		
		String query = "insert into admin (name, username, password, role)values('"+admin.getName()+"', '"+admin.getUsername()+"', '"+admin.getPassword()+"', '"+admin.getRole()+"')";
		db.addAndDeleteUser(query);		
		
	}

	@Override
	public void deleteUser(int id) {
		
		String query = "delete from admin where id = '"+id+"'";
		DbService db = new DbOps();
		db.addAndDeleteUser(query);
		
	}

	
	@Override
	public List<Admin> getAllUser() {
		
		DbService service = new DbOps();
		String query = "select * from admin";
		
		ResultSet rs = service.getUser(query);
		List<Admin> alist = new LinkedList<>();
		
		//Data Visualization
		try {
			while(rs.next()) {
				Admin a = new Admin();
				a.setId(rs.getInt("id"));
				a.setName(rs.getString("name"));
				a.setUsername(rs.getString("username"));
				a.setPassword(rs.getString("password"));
				a.setRole(rs.getString("role"));
				
				alist.add(a);				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return alist;
	}
	

}
