package com.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class index extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					index frame = new index();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public index() {
		setTitle("Index page");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 765, 463);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Admin Login");
		btnNewButton.setForeground(new Color(255, 215, 0));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new AdminLogin().setVisible(true);
				dispose();
			}
		});
		btnNewButton.setBackground(new Color(128, 128, 128));
		btnNewButton.setFont(new Font("Trebuchet MS", Font.BOLD, 24));
		btnNewButton.setBounds(514, 52, 195, 64);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Faculty Login");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				new FacultyLogin().setVisible(true);
				dispose();
			}
		});
		btnNewButton_1.setForeground(new Color(255, 215, 0));
		btnNewButton_1.setBackground(new Color(128, 128, 128));
		btnNewButton_1.setFont(new Font("Trebuchet MS", Font.BOLD, 24));
		btnNewButton_1.setBounds(514, 175, 195, 64);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Student Login");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new StudentLogin().setVisible(true);
				dispose();
			}
		});
		btnNewButton_2.setForeground(new Color(255, 215, 0));
		btnNewButton_2.setBackground(new Color(128, 128, 128));
		btnNewButton_2.setFont(new Font("Trebuchet MS", Font.BOLD, 24));
		btnNewButton_2.setBounds(514, 298, 195, 64);
		contentPane.add(btnNewButton_2);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(index.class.getResource("/resources/Bista_University_transparent-transformed (1).png")));
		lblNewLabel_1.setBounds(-30, 33, 566, 384);
		contentPane.add(lblNewLabel_1);
	}
}
