package com.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.model.Student;
import com.service.StudentService;
import com.service.StudentServiceImpl;

import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FacultyLogin extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField usernameTxt;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JPasswordField passwordTxt;
	private JButton LoginBtn;
	private JButton CancelBtn;
	private JButton RegisterBtn;
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FacultyLogin frame = new FacultyLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FacultyLogin() {
		setTitle("Faculty Login");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 577, 455);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getUsernameTxt());
		contentPane.add(getLblNewLabel());
		contentPane.add(getLblNewLabel_1());
		contentPane.add(getPasswordTxt());
		contentPane.add(getLoginBtn());
		contentPane.add(getCancelBtn());
		contentPane.add(getRegisterBtn());
		contentPane.add(getLblNewLabel_2());
	}

	private JTextField getUsernameTxt() {
		if (usernameTxt == null) {
			usernameTxt = new JTextField();
			usernameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			usernameTxt.setColumns(10);
			usernameTxt.setBackground(new Color(176, 196, 222));
			usernameTxt.setBounds(174, 121, 363, 38);
		}
		return usernameTxt;
	}
	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("Username");
			lblNewLabel.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel.setForeground(new Color(255, 215, 0));
			lblNewLabel.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblNewLabel.setBounds(10, 122, 154, 38);
		}
		return lblNewLabel;
	}
	private JLabel getLblNewLabel_1() {
		if (lblNewLabel_1 == null) {
			lblNewLabel_1 = new JLabel("Password");
			lblNewLabel_1.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_1.setForeground(new Color(255, 215, 0));
			lblNewLabel_1.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblNewLabel_1.setBounds(10, 199, 154, 38);
		}
		return lblNewLabel_1;
	}
	private JPasswordField getPasswordTxt() {
		if (passwordTxt == null) {
			passwordTxt = new JPasswordField();
			passwordTxt.setFont(new Font("Tahoma", Font.PLAIN, 18));
			passwordTxt.setBackground(new Color(176, 196, 222));
			passwordTxt.setBounds(174, 193, 363, 38);
		}
		return passwordTxt;
	}
	private JButton getLoginBtn() {
		if (LoginBtn == null) {
			LoginBtn = new JButton("Login");
			LoginBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					login();
				}
			});
			LoginBtn.setForeground(Color.GREEN);
			LoginBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			LoginBtn.setBackground(new Color(0, 128, 0));
			LoginBtn.setBounds(218, 265, 101, 46);
		}
		return LoginBtn;
	}
	private JButton getCancelBtn() {
		if (CancelBtn == null) {
			CancelBtn = new JButton("Cancel");
			CancelBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					clearField();
				}
			});
			CancelBtn.setForeground(new Color(139, 0, 0));
			CancelBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			CancelBtn.setBackground(Color.RED);
			CancelBtn.setBounds(389, 265, 101, 46);
		}
		return CancelBtn;
	}
	private JButton getRegisterBtn() {
		if (RegisterBtn == null) {
			RegisterBtn = new JButton("Click Here to Registery you Account");
			RegisterBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					new FacultyRegister().setVisible(true);
					dispose();
				}
			});
			RegisterBtn.setForeground(new Color(255, 215, 0));
			RegisterBtn.setFont(new Font("Berlin Sans FB", Font.PLAIN, 20));
			RegisterBtn.setBackground(Color.GRAY);
			RegisterBtn.setBounds(101, 345, 363, 38);
		}
		return RegisterBtn;
	}
	private JLabel getLblNewLabel_2() {
		if (lblNewLabel_2 == null) {
			lblNewLabel_2 = new JLabel("Faculty Login");
			lblNewLabel_2.setVerticalAlignment(SwingConstants.TOP);
			lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_2.setForeground(new Color(255, 215, 0));
			lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 36));
			lblNewLabel_2.setBounds(101, 34, 363, 53);
		}
		return lblNewLabel_2;
	}
	
private void clearField () {
		
		usernameTxt.setText("");
		passwordTxt.setText("");
	}
	
	private void login() {
		
		StudentService service = new StudentServiceImpl();					
		List<Student> list = new LinkedList<>();
		list = service.getAllStudent();
		
		boolean flag = false;
		
		for(Student a : list) {	
			
			if(usernameTxt.getText().equals(a.getUsername()) && passwordTxt.getText().equals(a.getPassword()) && a.getRole().equals("student")) {							
				flag = true;
				}
			};
			
			if(flag) {
				JOptionPane.showMessageDialog(null, "Login Sucess");
				new AdminDash().setVisible(true);
				dispose();
			} else {
				JOptionPane.showMessageDialog(null, "Incorrect Credential or Role");
				clearField();
			}
	}
}
