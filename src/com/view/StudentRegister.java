package com.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.model.Admin;
import com.model.Student;
import com.service.AdminService;
import com.service.AdminServiceImpl;
import com.service.StudentService;
import com.service.StudentServiceImpl;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StudentRegister extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblNewLabel_2_1;
	private JLabel lblFirstName;
	private JTextField ageTxt;
	private JLabel lblLastName;
	private JTextField majorTxt;
	private JLabel lblNewLabel_2;
	private JTextField usernameTxt;
	private JLabel lblPassword;
	private JPasswordField passwordTxt;
	private JLabel lblRetypePassword;
	private JPasswordField rpasswordTxt;
	private JButton registerBtn;
	private JButton rcancelBtn;
	private JButton sendLoginBtn;
	private JLabel lblFirstName_1;
	private JTextField nameTxt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentRegister frame = new StudentRegister();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentRegister() {
		setTitle("Student Register");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 832, 771);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNewLabel_2_1());
		contentPane.add(getLblFirstName());
		contentPane.add(getAgeTxt());
		contentPane.add(getLblLastName());
		contentPane.add(getMajorTxt());
		contentPane.add(getLblNewLabel_2());
		contentPane.add(getUsernameTxt());
		contentPane.add(getLblPassword());
		contentPane.add(getPasswordTxt());
		contentPane.add(getLblRetypePassword());
		contentPane.add(getRpasswordTxt());
		contentPane.add(getRegisterBtn());
		contentPane.add(getRcancelBtn());
		contentPane.add(getSendLoginBtn());
		contentPane.add(getLblFirstName_1());
		contentPane.add(getNameTxt());
	}

	private JLabel getLblNewLabel_2_1() {
		if (lblNewLabel_2_1 == null) {
			lblNewLabel_2_1 = new JLabel("Student Register");
			lblNewLabel_2_1.setVerticalAlignment(SwingConstants.TOP);
			lblNewLabel_2_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_2_1.setForeground(new Color(255, 215, 0));
			lblNewLabel_2_1.setFont(new Font("Tahoma", Font.BOLD, 36));
			lblNewLabel_2_1.setBounds(247, 37, 363, 46);
		}
		return lblNewLabel_2_1;
	}
	private JLabel getLblFirstName() {
		if (lblFirstName == null) {
			lblFirstName = new JLabel("Age");
			lblFirstName.setVerticalAlignment(SwingConstants.BOTTOM);
			lblFirstName.setHorizontalAlignment(SwingConstants.CENTER);
			lblFirstName.setForeground(new Color(255, 215, 0));
			lblFirstName.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblFirstName.setBounds(101, 202, 154, 38);
		}
		return lblFirstName;
	}
	private JTextField getAgeTxt() {
		if (ageTxt == null) {
			ageTxt = new JTextField();
			ageTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			ageTxt.setColumns(10);
			ageTxt.setBackground(new Color(176, 196, 222));
			ageTxt.setBounds(289, 195, 435, 38);
		}
		return ageTxt;
	}
	private JLabel getLblLastName() {
		if (lblLastName == null) {
			lblLastName = new JLabel("Major");
			lblLastName.setVerticalAlignment(SwingConstants.BOTTOM);
			lblLastName.setHorizontalAlignment(SwingConstants.CENTER);
			lblLastName.setForeground(new Color(255, 215, 0));
			lblLastName.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblLastName.setBounds(101, 277, 154, 38);
		}
		return lblLastName;
	}
	private JTextField getMajorTxt() {
		if (majorTxt == null) {
			majorTxt = new JTextField();
			majorTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			majorTxt.setColumns(10);
			majorTxt.setBackground(new Color(176, 196, 222));
			majorTxt.setBounds(289, 270, 435, 38);
		}
		return majorTxt;
	}
	private JLabel getLblNewLabel_2() {
		if (lblNewLabel_2 == null) {
			lblNewLabel_2 = new JLabel("Username");
			lblNewLabel_2.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_2.setForeground(new Color(255, 215, 0));
			lblNewLabel_2.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblNewLabel_2.setBounds(101, 352, 154, 38);
		}
		return lblNewLabel_2;
	}
	private JTextField getUsernameTxt() {
		if (usernameTxt == null) {
			usernameTxt = new JTextField();
			usernameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			usernameTxt.setColumns(10);
			usernameTxt.setBackground(new Color(176, 196, 222));
			usernameTxt.setBounds(289, 345, 435, 38);
		}
		return usernameTxt;
	}
	private JLabel getLblPassword() {
		if (lblPassword == null) {
			lblPassword = new JLabel("Password");
			lblPassword.setVerticalAlignment(SwingConstants.BOTTOM);
			lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
			lblPassword.setForeground(new Color(255, 215, 0));
			lblPassword.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblPassword.setBounds(101, 426, 154, 38);
		}
		return lblPassword;
	}
	private JPasswordField getPasswordTxt() {
		if (passwordTxt == null) {
			passwordTxt = new JPasswordField();
			passwordTxt.setFont(new Font("Tahoma", Font.PLAIN, 18));
			passwordTxt.setBackground(new Color(176, 196, 222));
			passwordTxt.setBounds(289, 420, 435, 38);
		}
		return passwordTxt;
	}
	private JLabel getLblRetypePassword() {
		if (lblRetypePassword == null) {
			lblRetypePassword = new JLabel("Retype Password");
			lblRetypePassword.setVerticalAlignment(SwingConstants.BOTTOM);
			lblRetypePassword.setHorizontalAlignment(SwingConstants.CENTER);
			lblRetypePassword.setForeground(new Color(255, 215, 0));
			lblRetypePassword.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblRetypePassword.setBounds(35, 501, 220, 38);
		}
		return lblRetypePassword;
	}
	private JPasswordField getRpasswordTxt() {
		if (rpasswordTxt == null) {
			rpasswordTxt = new JPasswordField();
			rpasswordTxt.setFont(new Font("Tahoma", Font.PLAIN, 18));
			rpasswordTxt.setBackground(new Color(176, 196, 222));
			rpasswordTxt.setBounds(289, 495, 435, 38);
		}
		return rpasswordTxt;
	}
	private JButton getRegisterBtn() {
		if (registerBtn == null) {
			registerBtn = new JButton("Register");
			registerBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					studentRegister();
				}
			});
			registerBtn.setForeground(Color.GREEN);
			registerBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			registerBtn.setBackground(new Color(0, 128, 0));
			registerBtn.setBounds(319, 570, 138, 46);
		}
		return registerBtn;
	}
	private JButton getRcancelBtn() {
		if (rcancelBtn == null) {
			rcancelBtn = new JButton("Cancel");
			rcancelBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					clearField();
				}
			});
			rcancelBtn.setForeground(new Color(139, 0, 0));
			rcancelBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			rcancelBtn.setBackground(Color.RED);
			rcancelBtn.setBounds(490, 570, 101, 46);
		}
		return rcancelBtn;
	}
	private JButton getSendLoginBtn() {
		if (sendLoginBtn == null) {
			sendLoginBtn = new JButton("Click Here to Login to you Account");
			sendLoginBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new StudentLogin().setVisible(true);
					dispose();
				}
			});
			sendLoginBtn.setForeground(new Color(255, 215, 0));
			sendLoginBtn.setFont(new Font("Berlin Sans FB", Font.PLAIN, 20));
			sendLoginBtn.setBackground(Color.GRAY);
			sendLoginBtn.setBounds(247, 653, 363, 38);
		}
		return sendLoginBtn;
	}
	private JLabel getLblFirstName_1() {
		if (lblFirstName_1 == null) {
			lblFirstName_1 = new JLabel("Name");
			lblFirstName_1.setVerticalAlignment(SwingConstants.BOTTOM);
			lblFirstName_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblFirstName_1.setForeground(new Color(255, 215, 0));
			lblFirstName_1.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblFirstName_1.setBounds(101, 127, 154, 38);
		}
		return lblFirstName_1;
	}
	private JTextField getNameTxt() {
		if (nameTxt == null) {
			nameTxt = new JTextField();
			nameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			nameTxt.setColumns(10);
			nameTxt.setBackground(new Color(176, 196, 222));
			nameTxt.setBounds(289, 120, 435, 38);
		}
		return nameTxt;
	}
	
	private void clearField() {
		
		nameTxt.setText("");
		ageTxt.setText("");
		usernameTxt.setText("");
		passwordTxt.setText("");
		rpasswordTxt.setText("");
		majorTxt.setText("");		
	};
	
	
	private void studentRegister () {

		Student a = new Student();
		StudentService as = new StudentServiceImpl();
		
		String name = nameTxt.getText();
		int age = Integer.parseInt(ageTxt.getText());
		String username = usernameTxt.getText();
		String password;
		String major = majorTxt.getText();		
		
		boolean flag = false;	
		
		if(!nameTxt.getText().equals(null) && !ageTxt.getText().equals(null) && !usernameTxt.getText().equals(null) && !passwordTxt.getText().equals(null) && !majorTxt.getText().equals(null) && !rpasswordTxt.getText().equals(null)) {
			flag = true;			
		} else {
			JOptionPane.showMessageDialog(null, "Please fill all Filds");
		};
		
		if(passwordTxt.getText().equals(rpasswordTxt.getText()) && flag) {
			
			password = rpasswordTxt.getText().trim();
			//major.toLowerCase();
			
			a.setName(name);
			a.setUsername(username);
			a.setPassword(password);
			a.setRole("student");
			a.setMajor(major);
			a.setAge(age);
			
			as.addStudent(a);
			
			JOptionPane.showMessageDialog(null, "User Added Sucess");
			
			clearField();
			
			
		} else {
			JOptionPane.showMessageDialog(null, "Password did not match");
		};			
		
	};
}
