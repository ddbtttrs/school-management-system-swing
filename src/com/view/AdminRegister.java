package com.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.db.DbOps;
import com.db.DbService;
import com.model.Admin;
import com.service.AdminService;
import com.service.AdminServiceImpl;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class AdminRegister extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblFirstName;
	private JTextField fnameTxt;
	private JLabel lblLastName;
	private JTextField lnameTxt;
	private JLabel lblNewLabel_2;
	private JTextField usernameTxt;
	private JLabel lblPassword;
	private JLabel lblRetypePassword;
	private JButton registerBtn;
	private JButton rcancelBtn;
	private JButton btnNewButton_2;
	private JLabel lblNewLabel_2_1;
	private JPasswordField passwordTxt;
	private JPasswordField rpasswordTxt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminRegister frame = new AdminRegister();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminRegister() {
		setTitle("Admin Form");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 832, 771);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblFirstName());
		contentPane.add(getFnameTxt());
		contentPane.add(getLblLastName());
		contentPane.add(getLnameTxt());
		contentPane.add(getLblNewLabel_2());
		contentPane.add(getUsernameTxt());
		contentPane.add(getLblPassword());
		contentPane.add(getLblRetypePassword());
		contentPane.add(getRegisterBtn());
		contentPane.add(getRcancelBtn());
		contentPane.add(getBtnNewButton_2());
		contentPane.add(getLblNewLabel_2_1());
		contentPane.add(getPasswordTxt());
		contentPane.add(getRpasswordTxt());
	}

	private JLabel getLblFirstName() {
		if (lblFirstName == null) {
			lblFirstName = new JLabel("First Name");
			lblFirstName.setVerticalAlignment(SwingConstants.BOTTOM);
			lblFirstName.setHorizontalAlignment(SwingConstants.CENTER);
			lblFirstName.setForeground(new Color(255, 215, 0));
			lblFirstName.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblFirstName.setBounds(106, 145, 154, 38);
		}
		return lblFirstName;
	}
	private JTextField getFnameTxt() {
		if (fnameTxt == null) {
			fnameTxt = new JTextField();
			fnameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			fnameTxt.setColumns(10);
			fnameTxt.setBackground(new Color(176, 196, 222));
			fnameTxt.setBounds(294, 138, 435, 38);
		}
		return fnameTxt;
	}
	private JLabel getLblLastName() {
		if (lblLastName == null) {
			lblLastName = new JLabel("Last Name");
			lblLastName.setVerticalAlignment(SwingConstants.BOTTOM);
			lblLastName.setHorizontalAlignment(SwingConstants.CENTER);
			lblLastName.setForeground(new Color(255, 215, 0));
			lblLastName.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblLastName.setBounds(106, 229, 154, 38);
		}
		return lblLastName;
	}
	private JTextField getLnameTxt() {
		if (lnameTxt == null) {
			lnameTxt = new JTextField();
			lnameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			lnameTxt.setColumns(10);
			lnameTxt.setBackground(new Color(176, 196, 222));
			lnameTxt.setBounds(294, 222, 435, 38);
		}
		return lnameTxt;
	}
	private JLabel getLblNewLabel_2() {
		if (lblNewLabel_2 == null) {
			lblNewLabel_2 = new JLabel("Username");
			lblNewLabel_2.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_2.setForeground(new Color(255, 215, 0));
			lblNewLabel_2.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblNewLabel_2.setBounds(106, 313, 154, 38);
		}
		return lblNewLabel_2;
	}
	private JTextField getUsernameTxt() {
		if (usernameTxt == null) {
			usernameTxt = new JTextField();
			usernameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			usernameTxt.setColumns(10);
			usernameTxt.setBackground(new Color(176, 196, 222));
			usernameTxt.setBounds(294, 306, 435, 38);
		}
		return usernameTxt;
	}
	private JLabel getLblPassword() {
		if (lblPassword == null) {
			lblPassword = new JLabel("Password");
			lblPassword.setVerticalAlignment(SwingConstants.BOTTOM);
			lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
			lblPassword.setForeground(new Color(255, 215, 0));
			lblPassword.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblPassword.setBounds(106, 397, 154, 38);
		}
		return lblPassword;
	}
	private JLabel getLblRetypePassword() {
		if (lblRetypePassword == null) {
			lblRetypePassword = new JLabel("Retype Password");
			lblRetypePassword.setVerticalAlignment(SwingConstants.BOTTOM);
			lblRetypePassword.setHorizontalAlignment(SwingConstants.CENTER);
			lblRetypePassword.setForeground(new Color(255, 215, 0));
			lblRetypePassword.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblRetypePassword.setBounds(40, 481, 220, 38);
		}
		return lblRetypePassword;
	}
	private JButton getRegisterBtn() {
		if (registerBtn == null) {
			registerBtn = new JButton("Register");
			registerBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					adminRegister();
				}
			});
			registerBtn.setForeground(Color.GREEN);
			registerBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			registerBtn.setBackground(new Color(0, 128, 0));
			registerBtn.setBounds(324, 558, 138, 46);
		}
		return registerBtn;
	}
	private JButton getRcancelBtn() {
		if (rcancelBtn == null) {
			rcancelBtn = new JButton("Cancel");
			rcancelBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					clear();
				}
			});
			rcancelBtn.setForeground(new Color(139, 0, 0));
			rcancelBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			rcancelBtn.setBackground(Color.RED);
			rcancelBtn.setBounds(495, 558, 101, 46);
		}
		return rcancelBtn;
	}
	private JButton getBtnNewButton_2() {
		if (btnNewButton_2 == null) {
			btnNewButton_2 = new JButton("Click Here to Login to you Account");
			btnNewButton_2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					new AdminLogin().setVisible(true);
					dispose();
					
				}
			});
			btnNewButton_2.setForeground(new Color(255, 215, 0));
			btnNewButton_2.setFont(new Font("Berlin Sans FB", Font.PLAIN, 20));
			btnNewButton_2.setBackground(Color.GRAY);
			btnNewButton_2.setBounds(228, 650, 363, 38);
		}
		return btnNewButton_2;
	}
	private JLabel getLblNewLabel_2_1() {
		if (lblNewLabel_2_1 == null) {
			lblNewLabel_2_1 = new JLabel("Admin Register");
			lblNewLabel_2_1.setVerticalAlignment(SwingConstants.TOP);
			lblNewLabel_2_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_2_1.setForeground(new Color(255, 215, 0));
			lblNewLabel_2_1.setFont(new Font("Tahoma", Font.BOLD, 36));
			lblNewLabel_2_1.setBounds(252, 46, 363, 46);
		}
		return lblNewLabel_2_1;
	}
	private JPasswordField getPasswordTxt() {
		if (passwordTxt == null) {
			passwordTxt = new JPasswordField();
			passwordTxt.setFont(new Font("Tahoma", Font.PLAIN, 18));
			passwordTxt.setBackground(new Color(176, 196, 222));
			passwordTxt.setBounds(294, 391, 435, 38);
		}
		return passwordTxt;
	}
	private JPasswordField getRpasswordTxt() {
		if (rpasswordTxt == null) {
			rpasswordTxt = new JPasswordField();
			rpasswordTxt.setFont(new Font("Tahoma", Font.PLAIN, 18));
			rpasswordTxt.setBackground(new Color(176, 196, 222));
			rpasswordTxt.setBounds(294, 474, 435, 38);
		}
		return rpasswordTxt;
	}
	
	private void adminRegister () {

		Admin a = new Admin();
		AdminService as = new AdminServiceImpl();
		String name = fnameTxt.getText() +" "+ lnameTxt.getText();
		String username = usernameTxt.getText();
		String password;
		boolean flag = false;
	
		
		if(!fnameTxt.getText().equals(null) && !lnameTxt.getText().equals(null) && !usernameTxt.getText().equals(null) && !passwordTxt.getText().equals(null) && !rpasswordTxt.getText().equals(null)) {
			flag = true;			
		} else {
			JOptionPane.showMessageDialog(null, "Please fill all Filds");
		};
		
		if(passwordTxt.getText().equals(rpasswordTxt.getText()) && flag) {
			
			password = rpasswordTxt.getText().trim();
			
			a.setName(name);
			a.setUsername(username);
			a.setPassword(password);
			a.setRole("admin");
			
			as.addUser(a);
			
			JOptionPane.showMessageDialog(null, "User Added Sucess");
			
			clear();
			
			
		} else {
			JOptionPane.showMessageDialog(null, "Password did not match");
		};			
		
	};
	
	
	private void clear() {
		
		fnameTxt.setText("");
		lnameTxt.setText("");
		usernameTxt.setText("");
		passwordTxt.setText("");
		rpasswordTxt.setText("");
		
	};
	
	
}
