package com.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.model.Faculty;
import com.model.Student;
import com.service.FacultyService;
import com.service.FacultyServiceImpl;
import com.service.StudentService;
import com.service.StudentServiceImpl;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FacultyRegister extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblNewLabel_2_1;
	private JLabel lblLastName_1;
	private JTextField lnameTxt;
	private JLabel lblLastName;
	private JTextField majorTxt;
	private JLabel lblNewLabel_2;
	private JTextField usernameTxt;
	private JLabel lblPassword;
	private JPasswordField passwordTxt;
	private JLabel lblRetypePassword;
	private JPasswordField rpasswordTxt;
	private JButton registerBtn;
	private JButton rcancelBtn;
	private JButton sendLoginBtn;
	private JLabel lblFirstName_1;
	private JTextField fnameTxt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FacultyRegister frame = new FacultyRegister();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FacultyRegister() {
		setTitle("Faculty Regiser");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 832, 711);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getLblNewLabel_2_1());
		contentPane.add(getLblLastName_1());
		contentPane.add(getLnameTxt());
		contentPane.add(getLblLastName());
		contentPane.add(getMajorTxt());
		contentPane.add(getLblNewLabel_2());
		contentPane.add(getUsernameTxt());
		contentPane.add(getLblPassword());
		contentPane.add(getPasswordTxt());
		contentPane.add(getLblRetypePassword());
		contentPane.add(getRpasswordTxt());
		contentPane.add(getRegisterBtn());
		contentPane.add(getRcancelBtn());
		contentPane.add(getSendLoginBtn());
		contentPane.add(getLblFirstName_1());
		contentPane.add(getFnameTxt());
	}

	private JLabel getLblNewLabel_2_1() {
		if (lblNewLabel_2_1 == null) {
			lblNewLabel_2_1 = new JLabel("Faculty Register");
			lblNewLabel_2_1.setVerticalAlignment(SwingConstants.TOP);
			lblNewLabel_2_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_2_1.setForeground(new Color(255, 215, 0));
			lblNewLabel_2_1.setFont(new Font("Tahoma", Font.BOLD, 36));
			lblNewLabel_2_1.setBounds(243, 11, 363, 46);
		}
		return lblNewLabel_2_1;
	}
	private JLabel getLblLastName_1() {
		if (lblLastName_1 == null) {
			lblLastName_1 = new JLabel("Last Name");
			lblLastName_1.setVerticalAlignment(SwingConstants.BOTTOM);
			lblLastName_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblLastName_1.setForeground(new Color(255, 215, 0));
			lblLastName_1.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblLastName_1.setBounds(97, 176, 154, 38);
		}
		return lblLastName_1;
	}
	private JTextField getLnameTxt() {
		if (lnameTxt == null) {
			lnameTxt = new JTextField();
			lnameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			lnameTxt.setColumns(10);
			lnameTxt.setBackground(new Color(176, 196, 222));
			lnameTxt.setBounds(285, 169, 435, 38);
		}
		return lnameTxt;
	}
	private JLabel getLblLastName() {
		if (lblLastName == null) {
			lblLastName = new JLabel("Major");
			lblLastName.setVerticalAlignment(SwingConstants.BOTTOM);
			lblLastName.setHorizontalAlignment(SwingConstants.CENTER);
			lblLastName.setForeground(new Color(255, 215, 0));
			lblLastName.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblLastName.setBounds(97, 251, 154, 38);
		}
		return lblLastName;
	}
	private JTextField getMajorTxt() {
		if (majorTxt == null) {
			majorTxt = new JTextField();
			majorTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			majorTxt.setColumns(10);
			majorTxt.setBackground(new Color(176, 196, 222));
			majorTxt.setBounds(285, 244, 435, 38);
		}
		return majorTxt;
	}
	private JLabel getLblNewLabel_2() {
		if (lblNewLabel_2 == null) {
			lblNewLabel_2 = new JLabel("Username");
			lblNewLabel_2.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_2.setForeground(new Color(255, 215, 0));
			lblNewLabel_2.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblNewLabel_2.setBounds(97, 326, 154, 38);
		}
		return lblNewLabel_2;
	}
	private JTextField getUsernameTxt() {
		if (usernameTxt == null) {
			usernameTxt = new JTextField();
			usernameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			usernameTxt.setColumns(10);
			usernameTxt.setBackground(new Color(176, 196, 222));
			usernameTxt.setBounds(285, 319, 435, 38);
		}
		return usernameTxt;
	}
	private JLabel getLblPassword() {
		if (lblPassword == null) {
			lblPassword = new JLabel("Password");
			lblPassword.setVerticalAlignment(SwingConstants.BOTTOM);
			lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
			lblPassword.setForeground(new Color(255, 215, 0));
			lblPassword.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblPassword.setBounds(97, 400, 154, 38);
		}
		return lblPassword;
	}
	private JPasswordField getPasswordTxt() {
		if (passwordTxt == null) {
			passwordTxt = new JPasswordField();
			passwordTxt.setFont(new Font("Tahoma", Font.PLAIN, 18));
			passwordTxt.setBackground(new Color(176, 196, 222));
			passwordTxt.setBounds(285, 394, 435, 38);
		}
		return passwordTxt;
	}
	private JLabel getLblRetypePassword() {
		if (lblRetypePassword == null) {
			lblRetypePassword = new JLabel("Retype Password");
			lblRetypePassword.setVerticalAlignment(SwingConstants.BOTTOM);
			lblRetypePassword.setHorizontalAlignment(SwingConstants.CENTER);
			lblRetypePassword.setForeground(new Color(255, 215, 0));
			lblRetypePassword.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblRetypePassword.setBounds(31, 475, 220, 38);
		}
		return lblRetypePassword;
	}
	private JPasswordField getRpasswordTxt() {
		if (rpasswordTxt == null) {
			rpasswordTxt = new JPasswordField();
			rpasswordTxt.setFont(new Font("Tahoma", Font.PLAIN, 18));
			rpasswordTxt.setBackground(new Color(176, 196, 222));
			rpasswordTxt.setBounds(285, 469, 435, 38);
		}
		return rpasswordTxt;
	}
	private JButton getRegisterBtn() {
		if (registerBtn == null) {
			registerBtn = new JButton("Register");
			registerBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					register();
					
				}
			});
			registerBtn.setForeground(Color.GREEN);
			registerBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			registerBtn.setBackground(new Color(0, 128, 0));
			registerBtn.setBounds(315, 544, 138, 46);
		}
		return registerBtn;
	}
	private JButton getRcancelBtn() {
		if (rcancelBtn == null) {
			rcancelBtn = new JButton("Cancel");
			rcancelBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					clearField();
					
				}
			});
			rcancelBtn.setForeground(new Color(139, 0, 0));
			rcancelBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			rcancelBtn.setBackground(Color.RED);
			rcancelBtn.setBounds(486, 544, 101, 46);
		}
		return rcancelBtn;
	}
	private JButton getSendLoginBtn() {
		if (sendLoginBtn == null) {
			sendLoginBtn = new JButton("Click Here to Login to you Account");
			sendLoginBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					new FacultyLogin().setVisible(true);
					dispose();
					
				}
			});
			sendLoginBtn.setForeground(new Color(255, 215, 0));
			sendLoginBtn.setFont(new Font("Berlin Sans FB", Font.PLAIN, 20));
			sendLoginBtn.setBackground(Color.GRAY);
			sendLoginBtn.setBounds(243, 627, 363, 38);
		}
		return sendLoginBtn;
	}
	private JLabel getLblFirstName_1() {
		if (lblFirstName_1 == null) {
			lblFirstName_1 = new JLabel("First Name");
			lblFirstName_1.setVerticalAlignment(SwingConstants.BOTTOM);
			lblFirstName_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblFirstName_1.setForeground(new Color(255, 215, 0));
			lblFirstName_1.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblFirstName_1.setBounds(97, 101, 154, 38);
		}
		return lblFirstName_1;
	}
	private JTextField getFnameTxt() {
		if (fnameTxt == null) {
			fnameTxt = new JTextField();
			fnameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			fnameTxt.setColumns(10);
			fnameTxt.setBackground(new Color(176, 196, 222));
			fnameTxt.setBounds(285, 94, 435, 38);
		}
		return fnameTxt;
	}
	
	private void clearField() {
		
		fnameTxt.setText("");
		lnameTxt.setText("");
		usernameTxt.setText("");
		passwordTxt.setText("");
		rpasswordTxt.setText("");
		majorTxt.setText("");		
	};
	
	
	private void register () {

		Faculty a = new Faculty();
		FacultyService as = new FacultyServiceImpl();
		
		String name = fnameTxt.getText()+" "+lnameTxt.getText();
		String username = usernameTxt.getText();
		String password;
		String major = majorTxt.getText();		
		
		boolean flag = false;	
		
		if(!fnameTxt.getText().equals(null) && !lnameTxt.getText().equals(null) && !usernameTxt.getText().equals(null) && !passwordTxt.getText().equals(null) && !majorTxt.getText().equals(null) && !rpasswordTxt.getText().equals(null)) {
			flag = true;			
		} else {
			JOptionPane.showMessageDialog(null, "Please fill all Filds");
		};
		
		if(passwordTxt.getText().equals(rpasswordTxt.getText()) && flag) {
			
			password = rpasswordTxt.getText().trim();
			//major.toLowerCase();
			
			a.setName(name);
			a.setUsername(username);
			a.setPassword(password);
			a.setRole("faculty");
			a.setMajor(major);
			
			as.addUser(a);
			
			JOptionPane.showMessageDialog(null, "User Added Sucess");
			
			clearField();
			
			
		} else {
			JOptionPane.showMessageDialog(null, "Password did not match");
		};			
		
	};
}
