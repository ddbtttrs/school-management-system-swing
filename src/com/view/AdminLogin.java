package com.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.model.Admin;
import com.service.AdminService;
import com.service.AdminServiceImpl;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.awt.event.ActionEvent;

public class AdminLogin extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField usernameTxt;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JPasswordField passwordTxt;
	private JButton aLoginBtn;
	private JButton aCancelBtn;
	private JButton aRegisterBtn;
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminLogin frame = new AdminLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminLogin() {
		setFont(new Font("Bodoni MT", Font.PLAIN, 20));
		setForeground(Color.ORANGE);
		setTitle("Admin Login");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 577, 455);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getUsernameTxt());
		contentPane.add(getLblNewLabel());
		contentPane.add(getLblNewLabel_1());
		contentPane.add(getPasswordTxt());
		contentPane.add(getALoginBtn());
		contentPane.add(getACancelBtn());
		contentPane.add(getARegisterBtn());
		contentPane.add(getLblNewLabel_2());
	}
	private JTextField getUsernameTxt() {
		if (usernameTxt == null) {
			usernameTxt = new JTextField();
			usernameTxt.setFont(new Font("Trebuchet MS", Font.BOLD, 23));
			usernameTxt.setBackground(new Color(176, 196, 222));
			usernameTxt.setBounds(174, 96, 363, 38);
			usernameTxt.setColumns(10);
		}
		return usernameTxt;
	}
	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("Username");
			lblNewLabel.setForeground(new Color(255, 215, 0));
			lblNewLabel.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel.setBounds(10, 99, 154, 38);
		}
		return lblNewLabel;
	}
	private JLabel getLblNewLabel_1() {
		if (lblNewLabel_1 == null) {
			lblNewLabel_1 = new JLabel("Password");
			lblNewLabel_1.setForeground(new Color(255, 215, 0));
			lblNewLabel_1.setVerticalAlignment(SwingConstants.BOTTOM);
			lblNewLabel_1.setFont(new Font("Yu Gothic", Font.BOLD, 24));
			lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_1.setBounds(10, 200, 154, 38);
		}
		return lblNewLabel_1;
	}
	private JPasswordField getPasswordTxt() {
		if (passwordTxt == null) {
			passwordTxt = new JPasswordField();
			passwordTxt.setFont(new Font("Tahoma", Font.PLAIN, 18));
			passwordTxt.setBackground(new Color(176, 196, 222));
			passwordTxt.setBounds(174, 197, 363, 38);
		}
		return passwordTxt;
	}
	private JButton getALoginBtn() {
		if (aLoginBtn == null) {
			aLoginBtn = new JButton("Login");
			aLoginBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					AdminService service = new AdminServiceImpl();					
					List<Admin> alist = new LinkedList<>();
					alist = service.getAllUser();
					boolean flag = false;
					
					for(Admin a : alist) {	
						
						if(usernameTxt.getText().equals(a.getUsername()) && passwordTxt.getText().equals(a.getPassword()) && a.getRole().equals("admin")) {							
							flag = true;
							}
						};
						
						if(flag) {
							new AdminDash().setVisible(true);
							dispose();
						} else {
							JOptionPane.showMessageDialog(null, "Incorrect Credential or Role");
							usernameTxt.setText("");
							passwordTxt.setText("");
						}
						
					}
					
			});
			aLoginBtn.setBackground(new Color(0, 128, 0));
			aLoginBtn.setForeground(new Color(0, 255, 0));
			aLoginBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			aLoginBtn.setBounds(218, 256, 101, 46);
		}
		return aLoginBtn;
	}
	private JButton getACancelBtn() {
		if (aCancelBtn == null) {
			aCancelBtn = new JButton("Cancel");
			aCancelBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					usernameTxt.setText("");
					passwordTxt.setText("");
				}
			});
			aCancelBtn.setBackground(new Color(255, 0, 0));
			aCancelBtn.setForeground(new Color(139, 0, 0));
			aCancelBtn.setFont(new Font("Trebuchet MS", Font.PLAIN, 20));
			aCancelBtn.setBounds(389, 256, 101, 46);
		}
		return aCancelBtn;
	}
	private JButton getARegisterBtn() {
		if (aRegisterBtn == null) {
			aRegisterBtn = new JButton("Click Here to Registery you Account");
			aRegisterBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					new AdminRegister().setVisible(true);
					dispose();
				}
			});
			aRegisterBtn.setForeground(new Color(255, 215, 0));
			aRegisterBtn.setBackground(Color.GRAY);
			aRegisterBtn.setFont(new Font("Berlin Sans FB", Font.PLAIN, 20));
			aRegisterBtn.setBounds(101, 329, 363, 38);
		}
		return aRegisterBtn;
	}
	private JLabel getLblNewLabel_2() {
		if (lblNewLabel_2 == null) {
			lblNewLabel_2 = new JLabel("Student Login");
			lblNewLabel_2.setForeground(new Color(255, 215, 0));
			lblNewLabel_2.setVerticalAlignment(SwingConstants.TOP);
			lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 36));
			lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_2.setBounds(101, 11, 363, 74);
		}
		return lblNewLabel_2;
	}
}
