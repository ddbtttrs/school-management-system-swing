package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.model.Admin;
import com.model.Student;
import com.mysql.cj.jdbc.Driver;

public class DbOps implements DbService {

	public Connection dbConnect() {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/testdb", "sbista", "Nqabccfi@7");
			
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
		
	}	

	@Override
	public void addAndDeleteUser(String query) {
		
		Statement stm;
		try {
			stm = this.dbConnect().createStatement();
			stm.execute(query);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override	
	public ResultSet getUser(String query) {
		try {
			
			Statement stm = this.dbConnect().createStatement();
			return stm.executeQuery(query);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
		
	}	
	
}
